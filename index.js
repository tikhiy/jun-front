const { resolve } = require('path');

const server = require('json-server');

const app = server.create();

app
  .use(
    server.defaults({
      static: resolve('dist'),
    })
  )
  .use(server.rewriter(require('./routes.json')))
  .use(server.router('db.json'));

app.listen(process.env.PORT);
