import { TextDecoder, TextEncoder } from 'text-encoding';

if (typeof global.TextEncoder !== 'function') {
  global.TextEncoder = TextEncoder;
  global.TextDecoder = TextDecoder;
}
