import './encoding';
import 'core-js';
import 'normalize.css';

import React, { FC } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';

import Employees from 'components/Employees';
import ErrorModal from 'components/ErrorModal';
import GlobalStyle from 'components/GlobalStyle';
import { Grid } from 'components/Grid';
import Headline from 'components/Headline';
import Overlay from 'components/Overlay';
import RevealCreateEmployee from 'components/RevealCreateEmployee';
import Toasts from 'components/Toasts';

import store from 'core/store';
import createTheme from 'core/theme/createTheme';

const theme = createTheme('Dark');

const App: FC = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Overlay />
      <Toasts />

      <Grid>
        <ErrorModal />

        <Headline dense as="div">
          <h1>Список сотрудников</h1>
          <RevealCreateEmployee autoFocus />
        </Headline>

        <Employees />
      </Grid>
    </ThemeProvider>
  </Provider>
);

render(<App />, document.getElementById('app'));
