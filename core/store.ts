import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk, { ThunkAction } from 'redux-thunk';

import { interceptors } from './axiosInstance';
import employee, { EmployeeAction } from './features/employee/reducers';
import employees, { EmployeesAction } from './features/employees/reducers';
import error, { ErrorAction } from './features/error/reducers';
import modals, { ModalsAction } from './features/modals/reducers';
import toasts, { ToastsAction } from './features/toasts/reducers';

const reducer = combineReducers({
  employee,
  employees,
  error,
  modals,
  toasts,
});

const store = createStore(reducer, applyMiddleware(thunk));

interceptors(store);

export default store;

export type RootState = ReturnType<typeof reducer>;

export type AppDispatch = typeof store.dispatch;

export type AppThunk = ThunkAction<
  void,
  RootState,
  unknown,
  EmployeeAction | EmployeesAction | ErrorAction | ModalsAction | ToastsAction
>;
