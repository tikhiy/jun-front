import { DefaultTheme } from 'styled-components';

import { Alpha, Color, State } from './DefaultTheme';

// properties
type P = [string, string, string, string];

// values
type E = [number, number, number, number];

/**
 * Constructs CSS-valid longhand properties.
 * @see Theme#rounded for an example.
 */
const buildLonghandProperties = (edges: E, props: P, value: string): string => {
  const [t, r, b, l] = edges;

  return `
    ${t ? `${props[0]}: ${value};` : ''}
    ${r ? `${props[1]}: ${value};` : ''}
    ${b ? `${props[2]}: ${value};` : ''}
    ${l ? `${props[3]}: ${value};` : ''}
  `;
};

class Theme implements DefaultTheme {
  borderRadius: DefaultTheme['borderRadius'];
  borderWidth: DefaultTheme['borderWidth'];
  gutter: DefaultTheme['gutter'];
  palette: DefaultTheme['palette'];
  states: DefaultTheme['states'];
  theme: DefaultTheme['theme'];

  constructor(
    properties: Pick<
      DefaultTheme,
      'borderRadius' | 'borderWidth' | 'gutter' | 'palette' | 'states' | 'theme'
    >
  ) {
    const {
      borderRadius,
      borderWidth,
      gutter,
      palette,
      states,
      theme,
    } = properties;

    this.borderRadius = borderRadius;
    this.borderWidth = borderWidth;
    this.gutter = gutter;
    this.palette = palette;
    this.states = states;
    this.theme = theme;
  }

  /**
   * Constructs CSS-valid `border` property based on `borderWidth`.
   */
  bordered(color: Color, alpha?: Alpha): string {
    return `border: ${this.borderWidth} solid ${this.color(color, alpha)};`;
  }

  /**
   * Constructs CSS-valid `border-radius` property based on `borderRadius`.
   */
  rounded(edges?: [number, number, number, number]): string {
    const r = this.borderRadius;

    if (edges) {
      return buildLonghandProperties(
        edges,
        [
          'border-top-left-radius',
          'border-top-right-radius',
          'border-bottom-right-radius',
          'border-bottom-left-radius',
        ],
        r
      );
    }

    return `border-radius: ${r};`;
  }

  /**
   * Returns CSS-valid and consistent spacing based on `gutter` and `multiplier`.
   */
  spacing(multiplier: number): string {
    return `${this.gutter * multiplier}px`;
  }

  /**
   * Constructs CSS-valid color.
   */
  color(color: Color, alpha?: Alpha): string {
    // in case color argument is a ColorName (defined in DefaultTheme.d.ts)
    const value =
      typeof color === 'string' ? this.palette[color][this.theme] : color;

    const [r, g, b] = value;

    // in case alpha argument was not specified
    if (typeof alpha === 'undefined') {
      // fallback it to either color argument's alpha or 1
      alpha =
        typeof value[3] === 'undefined'
          ? // fallback to 1
            1
          : // fallback to color's alpha (4th value)
            value[3];
    }

    // in case alpha is a State (defined in DefaultTheme.d.ts)
    const a = typeof alpha === 'number' ? alpha : this.state(alpha);

    return `rgba(${r}, ${g}, ${b}, ${a})`;
  }

  /**
   * Returns RGBA-valid alpha value from `states`.
   */
  state(state: State): number {
    return this.states[state][this.theme];
  }
}

export default Theme;
