import { DefaultTheme } from 'styled-components';

import Theme from './Theme';

const createTheme = (theme: DefaultTheme['theme']): DefaultTheme => {
  return new Theme({
    borderRadius: '4px',
    borderWidth: '1px',
    gutter: 8,

    palette: {
      primary: {
        Dark: [153, 153, 255],
      },

      success: {
        Dark: [153, 255, 153],
      },

      warning: {
        Dark: [255, 204, 153],
      },

      danger: {
        Dark: [255, 153, 153],
      },

      bg: {
        Dark: [18, 18, 18],
      },

      fg: {
        Dark: [30, 30, 30],
      },

      toast: {
        Dark: [12, 12, 12],
      },

      border: {
        Dark: [47, 47, 47],
      },

      highEmphasis: {
        Dark: [255, 255, 255, 0.87],
      },

      mediumEmphasis: {
        Dark: [255, 255, 255, 0.6],
      },
    },

    states: {
      enabled: { Dark: 0.08 },
      hovered: { Dark: 0.12 },
      pressed: { Dark: 0.16 },
    },

    theme,
  });
};

export default createTheme;
