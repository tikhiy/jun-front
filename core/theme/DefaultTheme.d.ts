import 'styled-components';

type State = 'enabled' | 'hovered' | 'pressed';

type ColorName =
  | 'primary'
  | 'success'
  | 'warning'
  | 'danger'
  | 'bg'
  | 'fg'
  | 'toast'
  | 'border'
  | 'highEmphasis'
  | 'mediumEmphasis';

type Alpha = number | State;

type ColorValue = [number, number, number, Alpha?];

type Color = ColorName | ColorValue;

type ThemeMode = 'Dark';

declare module 'styled-components' {
  export interface DefaultTheme {
    borderRadius: string;
    borderWidth: string;
    gutter: number;

    palette: {
      [C in ColorName]: {
        [T in ThemeMode]: ColorValue;
      };
    };

    states: {
      [S in State]: {
        [T in ThemeMode]: number;
      };
    };

    theme: ThemeMode;

    bordered(this: DefaultTheme, color: Color, alpha?: Alpha): string;
    rounded(
      this: DefaultTheme,
      edges?: [number, number, number, number]
    ): string;
    spacing(this: DefaultTheme, multiplier: number): string;
    color(this: DefaultTheme, color: Color, alpha?: Alpha): string;
    state(this: DefaultTheme, state: State): number;
  }
}
