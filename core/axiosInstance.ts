import Axios, { AxiosError } from 'axios';
import { Store } from 'redux';

import { setError } from './features/error/actions';
import { addToast } from './features/toasts/actions';

const axiosInstance = Axios.create({
  baseURL: process.env.BASE_URL,
});

const interceptors = (store: Store): void => {
  const onRejected = ({ response }: AxiosError) => {
    let error = 'Ошибка!';

    switch (response?.status) {
      case 400:
        error = 'Не удалось обработать запрос.';
        break;
      case 404:
        error = 'Не удалось найти запрашиваемый ресурс.';
        break;
      case 500:
        error = 'Произошла непредвиденная ошибка.';
        break;
    }

    store.dispatch(setError(error));
    store.dispatch(addToast(error, 'danger'));

    throw error;
  };

  axiosInstance.interceptors.response.use(null, onRejected);
};

export default axiosInstance;

export { interceptors };
