import useBreakpoint, { LG, MD, SM, XL } from './useBreakpoint';

export type IBreakpointSwitch<T> = {
  xl: T;
  lg: T;
  md: T;
  sm: T;
  xs: T;
};

const useBreakpointSwitch = <T>({
  xl,
  lg,
  md,
  sm,
  xs,
}: Partial<IBreakpointSwitch<T>>): T | undefined => {
  const breakpoint = useBreakpoint();

  switch (breakpoint) {
    case XL:
      if (typeof xl !== 'undefined') {
        return xl;
      }
    /* fallthrough */
    case LG:
      if (typeof lg !== 'undefined') {
        return lg;
      }
    /* fallthrough */
    case MD:
      if (typeof md !== 'undefined') {
        return md;
      }
    /* fallthrough */
    case SM:
      if (typeof sm !== 'undefined') {
        return sm;
      }
  }

  return xs;
};

export default useBreakpointSwitch;
