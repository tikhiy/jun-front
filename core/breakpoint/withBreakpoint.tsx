import React, { ComponentType, FC } from 'react';

import useBreakpoint from './useBreakpoint';

export type BreakpointProps = { breakpoint: number };

type T<P> = P extends infer U & BreakpointProps
  ? U extends BreakpointProps
    ? Record<string, never>
    : U
  : never;

const withBreakpoint = <P extends BreakpointProps>(
  Component: ComponentType<P>
): FC<T<P>> => {
  const HOC: FC<T<P>> = function HOC(props) {
    const breakpoint = useBreakpoint();

    // eslint-disable-next-line
    // @ts-ignore
    return <Component {...props} breakpoint={breakpoint} />;
  };

  HOC.displayName = `withBreakpoint(${
    Component.displayName || Component.name
  })`;

  return HOC;
};

export default withBreakpoint;
