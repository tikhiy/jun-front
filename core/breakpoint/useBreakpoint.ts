import { useEffect, useState } from 'react';

export const XL = 1200;
export const LG = 992;
export const MD = 768;
export const SM = 576;
export const XS = 0;

const computeBreakpoint = (): number => {
  const w = innerWidth;

  switch (true) {
    case w >= XL:
      return XL;
    case w >= LG:
      return LG;
    case w >= MD:
      return MD;
    case w >= SM:
      return SM;
  }

  return XS;
};

const useBreakpoint = (): number => {
  const [breakpoint, setBreakpoint] = useState<number>(computeBreakpoint());

  useEffect(() => {
    const onResize = (): void => {
      const newBreakpoint = computeBreakpoint();

      if (newBreakpoint !== breakpoint) {
        setBreakpoint(newBreakpoint);
      }
    };

    window.addEventListener('resize', onResize);

    return (): void => {
      window.removeEventListener('resize', onResize);
    };
  });

  return breakpoint;
};

export default useBreakpoint;
