import { Employee } from 'core/Employee';

export const SET_EMPLOYEE = 'SET_EMPLOYEE';

export type EmployeeState = null | Employee;
