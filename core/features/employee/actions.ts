import { Employee } from 'core/Employee';

import { SET_EMPLOYEE } from './types';

const setEmployee = (employee: Employee) => ({
  type: SET_EMPLOYEE,
  employee,
});

export { setEmployee };
