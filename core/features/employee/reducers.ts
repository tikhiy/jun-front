import { InferActionCreatorType } from 'core/InferActionCreatorType';

import * as actions from './actions';
import { EmployeeState, SET_EMPLOYEE } from './types';

export type EmployeeAction = ReturnType<InferActionCreatorType<typeof actions>>;

const initialState: EmployeeState = null;

const employee = (
  state: EmployeeState = initialState,
  action: EmployeeAction
): EmployeeState => {
  switch (action.type) {
    case SET_EMPLOYEE:
      return action.employee;
    default:
      return state;
  }
};

export default employee;
