import { HIDE_MODAL, ModalsVariant, SHOW_MODAL } from './types';

const showModal = (variant: ModalsVariant) => ({
  type: SHOW_MODAL,
  variant,
});

const hideModal = () => ({
  type: HIDE_MODAL,
});

export { hideModal, showModal };
