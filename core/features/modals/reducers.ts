import { InferActionCreatorType } from 'core/InferActionCreatorType';

import * as actions from './actions';
import { HIDE_MODAL, ModalsState, SHOW_MODAL } from './types';

export type ModalsAction = ReturnType<InferActionCreatorType<typeof actions>>;

const initialState: ModalsState = [];

const modals = (state = initialState, action: ModalsAction): ModalsState => {
  switch (action.type) {
    case SHOW_MODAL:
      return state.concat(action.variant);
    case HIDE_MODAL:
      return state.slice(0, -1);
    default:
      return state;
  }
};

export default modals;
