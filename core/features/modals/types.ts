export const SHOW_MODAL = 'SHOW_MODAL' as const;
export const HIDE_MODAL = 'HIDE_MODAL' as const;

export type ModalsVariant =
  | 'EMPLOYEE_ACTIONS_MODAL'
  | 'CREATE_EMPLOYEE_MODAL'
  | 'UPDATE_EMPLOYEE_MODAL'
  | 'DELETE_EMPLOYEE_MODAL'
  | 'ERROR_MODAL';

export type ModalsState = ModalsVariant[];
