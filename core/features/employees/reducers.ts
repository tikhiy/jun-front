import { InferActionCreatorType } from 'core/InferActionCreatorType';

import * as actions from './actions';
import {
  CREATE_EMPLOYEE_SUCCESS,
  DELETE_EMPLOYEE_SUCCESS,
  EmployeesState,
  FETCH_EMPLOYEES_SUCCESS,
  UPDATE_EMPLOYEE_SUCCESS,
} from './types';

export type EmployeesAction = ReturnType<
  InferActionCreatorType<typeof actions>
>;

const initialState: EmployeesState = null;

const employees = (
  state = initialState,
  action: EmployeesAction
): EmployeesState => {
  switch (action.type) {
    case FETCH_EMPLOYEES_SUCCESS:
      return action.employees;
    case CREATE_EMPLOYEE_SUCCESS:
      if (state) {
        return state.concat(action.employee);
      }

      return [action.employee];
    case UPDATE_EMPLOYEE_SUCCESS:
      if (state) {
        return state.map((employee) =>
          employee.id === action.employee.id ? action.employee : employee
        );
      }

      return [action.employee];
    case DELETE_EMPLOYEE_SUCCESS:
      if (state) {
        state = state.filter(({ id }) => id !== action.id);
      }

      return state;
    default:
      return state;
  }
};

export default employees;
