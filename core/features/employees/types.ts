import { Employee } from 'core/Employee';

export const FETCH_EMPLOYEES_SUCCESS = 'FETCH_EMPLOYEES_SUCCESS' as const;
export const FETCH_EMPLOYEES = 'FETCH_EMPLOYEES' as const;
export const CREATE_EMPLOYEE_SUCCESS = 'CREATE_EMPLOYEE_SUCCESS' as const;
export const CREATE_EMPLOYEE = 'CREATE_EMPLOYEE' as const;
export const UPDATE_EMPLOYEE_SUCCESS = 'UPDATE_EMPLOYEE_SUCCESS' as const;
export const UPDATE_EMPLOYEE = 'UPDATE_EMPLOYEE' as const;
export const DELETE_EMPLOYEE_SUCCESS = 'DELETE_EMPLOYEE_SUCCESS' as const;
export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE' as const;

export type EmployeesState = null | Employee[];
