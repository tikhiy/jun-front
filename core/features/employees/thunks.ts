import { Employee } from 'core/Employee';
import axiosInstance from 'core/axiosInstance';
import { AppThunk } from 'core/store';

import { hideModal } from '../modals/actions';

import {
  createEmployeeSuccess,
  deleteEmployeeSuccess,
  fetchEmployeesSuccess,
  updateEmployeeSuccess,
} from './actions';

const fetchEmployees = (): AppThunk => async (dispatch) => {
  const { data } = await axiosInstance.get<Employee[]>('/persons');

  dispatch(fetchEmployeesSuccess(data));
};

const createEmployee = (employee: Omit<Employee, 'id'>): AppThunk => async (
  dispatch
) => {
  const { data } = await axiosInstance.post<Employee>('/person', employee);

  dispatch(createEmployeeSuccess(data));
  dispatch(hideModal());
};

const updateEmployee = (employee: Employee): AppThunk => async (dispatch) => {
  const { firstName, lastName, id } = employee;

  const { data } = await axiosInstance.put<Employee>(`/person/${id}`, {
    firstName,
    lastName,
  });

  dispatch(updateEmployeeSuccess(data));
  dispatch(hideModal());
};

const deleteEmployee = (id: Employee['id']): AppThunk => async (dispatch) => {
  await axiosInstance.delete(`/person/${id}`);
  dispatch(deleteEmployeeSuccess(id));
  dispatch(hideModal());
};

export { fetchEmployees, createEmployee, updateEmployee, deleteEmployee };
