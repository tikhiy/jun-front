import { Employee } from 'core/Employee';

import {
  CREATE_EMPLOYEE_SUCCESS,
  DELETE_EMPLOYEE_SUCCESS,
  FETCH_EMPLOYEES_SUCCESS,
  UPDATE_EMPLOYEE_SUCCESS,
} from './types';

const fetchEmployeesSuccess = (employees: Employee[]) => ({
  type: FETCH_EMPLOYEES_SUCCESS,
  employees,
});

const createEmployeeSuccess = (employee: Employee) => ({
  type: CREATE_EMPLOYEE_SUCCESS,
  employee,
});

const updateEmployeeSuccess = (employee: Employee) => ({
  type: UPDATE_EMPLOYEE_SUCCESS,
  employee,
});

const deleteEmployeeSuccess = (id: Employee['id']) => ({
  type: DELETE_EMPLOYEE_SUCCESS,
  id,
});

export {
  fetchEmployeesSuccess,
  createEmployeeSuccess,
  updateEmployeeSuccess,
  deleteEmployeeSuccess,
};
