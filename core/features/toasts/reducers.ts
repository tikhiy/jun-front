import { InferActionCreatorType } from 'core/InferActionCreatorType';

import * as actions from './actions';
import { ADD_TOAST, REMOVE_TOAST, ToastsState } from './types';

export type ToastsAction = ReturnType<InferActionCreatorType<typeof actions>>;

const initialState: ToastsState = [];

let id = 0;

const toasts = (state = initialState, action: ToastsAction): ToastsState => {
  switch (action.type) {
    case ADD_TOAST:
      return state.concat({
        color: action.color,
        textContent: action.textContent,
        id: ++id,
      });
    case REMOVE_TOAST:
      return state.filter(({ id }) => id !== action.toast.id);
    default:
      return state;
  }
};

export default toasts;
