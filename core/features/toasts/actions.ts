import { Color } from 'core/theme/DefaultTheme';

import { ADD_TOAST, REMOVE_TOAST, Toast } from './types';

const addToast = (textContent: string, color: Color) => ({
  type: ADD_TOAST,
  color,
  textContent,
});

const removeToast = (toast: Toast) => ({
  type: REMOVE_TOAST,
  toast,
});

export { addToast, removeToast };
