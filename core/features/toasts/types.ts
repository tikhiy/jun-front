import { Color } from 'core/theme/DefaultTheme';

export const ADD_TOAST = 'ADD_TOAST' as const;
export const REMOVE_TOAST = 'REMOVE_TOAST' as const;

export type Toast = {
  color: Color;
  textContent: string;
  id: number;
};

export type ToastsState = Toast[];
