import { InferActionCreatorType } from 'core/InferActionCreatorType';

import * as actions from './actions';
import { ErrorState, SET_ERROR } from './types';

export type ErrorAction = ReturnType<InferActionCreatorType<typeof actions>>;

const initialState: ErrorState = '';

const error = (state = initialState, action: ErrorAction): ErrorState => {
  switch (action.type) {
    case SET_ERROR:
      return action.error;
    default:
      return state;
  }
};

export default error;
