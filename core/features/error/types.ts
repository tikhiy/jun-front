export const SET_ERROR = 'SET_ERROR' as const;

export type ErrorState = string;
