import { SET_ERROR } from './types';

const setError = (error: string) => ({
  type: SET_ERROR,
  error,
});

export { setError };
