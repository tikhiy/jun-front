export type InferActionCreatorType<T> = T extends { [K: string]: infer U }
  ? U
  : never;
