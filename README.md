# JunFront

Heroku приложение: https://jun-front.herokuapp.com.

- __Адаптивный__ дизайн
- Поддержка __Internet Explorer 11__
- Написано на __TypeScript__
- Сделано с __React__ и __Redux__
- Сборка с __webpack__
- Проверка кода с __ESLint__ и __Prettier__ (_Standard Style Guide_)

## Запуск

| - | Команда |
| - | -- |
| Установка зависимостей | `yarn` |
| Устаровка `production` режима | `export NODE_ENV=production` |
| Сборка | `yarn build` |
| Запуск | `yarn start` |

## VSC расширения для разработки

| Название | ID |
| - | - |
| JavaScript and TypeScript Nightly | `ms-vscode.vscode-typescript-next` |
| ESLint | `dbaeumer.vscode-eslint` |
| vscode-styled-components | `jpoissonnier.vscode-styled-components` |
| Code Spell Checker | `streetsidesoftware.code-spell-checker` |
