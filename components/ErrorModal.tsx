import React, { FC } from 'react';
import { useSelector } from 'react-redux';

import Headline from 'components/Headline';

import { ErrorState } from 'core/features/error/types';
import { RootState } from 'core/store';

import CancelButton from './CancelButton';
import Modal from './Modal';

const ErrorModal: FC = () => {
  const error = useSelector<RootState, ErrorState>(({ error }) => error);

  return (
    <Modal variant="ERROR_MODAL">
      <Headline>{error || 'Ошибка!'}</Headline>
      <CancelButton />
    </Modal>
  );
};

export default ErrorModal;
