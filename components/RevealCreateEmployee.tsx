import React, {
  ButtonHTMLAttributes,
  DetailedHTMLProps,
  FC,
  useMemo,
} from 'react';
import { useDispatch } from 'react-redux';

import { showModal } from 'core/features/modals/actions';
import { AppDispatch } from 'core/store';

import Button from './Button';

const RevealCreateEmployee: FC<
  DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>
> = (props) => {
  const dispatch = useDispatch<AppDispatch>();

  const onClick = useMemo(
    () => () => dispatch(showModal('CREATE_EMPLOYEE_MODAL')),
    [dispatch]
  );

  return (
    <Button {...props} color="primary" onClick={onClick}>
      Добавить сотрудника
    </Button>
  );
};

export default RevealCreateEmployee;
