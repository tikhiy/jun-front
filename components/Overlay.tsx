import { connect } from 'react-redux';
import styled, { CSSProperties } from 'styled-components';

import { RootState } from 'core/store';

const Overlay = styled.div<{ active: boolean }>`
  visibility: ${({ active }): CSSProperties['visibility'] =>
    active ? 'visible' : 'hidden'};

  transition: ${({ active }): CSSProperties['transition'] =>
    active
      ? 'visibility 0s 0.000s, opacity 0.125s 0s'
      : 'visibility 0s 0.125s, opacity 0.125s 0s'};

  opacity: ${({ active }): CSSProperties['opacity'] => (active ? 0.875 : 0)};

  background-color: black;
  position: fixed;
  bottom: 0;
  right: 0;
  left: 0;
  top: 0;
  z-index: 1;
`;

const mapState = ({ modals }: RootState) => ({
  active: modals.length > 0,
});

export default connect(mapState)(Overlay);
