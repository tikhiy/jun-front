import { ComponentType, DetailedHTMLProps, HTMLAttributes } from 'react';
import styled, { css } from 'styled-components';

import { SM } from 'core/breakpoint/useBreakpoint';
import withBreakpoint, {
  BreakpointProps,
} from 'core/breakpoint/withBreakpoint';

type P = {
  dense?: boolean;
  as?: string | ComponentType<unknown>;
} & DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;

const Headline = withBreakpoint<P & BreakpointProps>(styled.h1<
  P & BreakpointProps
>`
  ${({ theme, dense }) =>
    dense
      ? ''
      : css`
          margin-right: ${theme.spacing(1)};
          margin-left: ${theme.spacing(1)};
          width: calc(100% - ${theme.spacing(2)});
        `}

  justify-content: space-between;
  justify-items: center;
  display: flex;
  align-items: center;

  ${({ theme, dense, breakpoint }) => {
    if (breakpoint < SM) {
      return css`
        margin: ${dense ? theme.spacing(2) : theme.spacing(1)};
        flex-direction: row;
        flex-wrap: wrap;

        & * {
          flex-basis: 100%;
        }
      `;
    }

    return '';
  }}
`);

export default Headline;
