import React, {
  ButtonHTMLAttributes,
  DetailedHTMLProps,
  FC,
  useMemo,
} from 'react';
import { useDispatch } from 'react-redux';

import { hideModal } from 'core/features/modals/actions';
import { AppDispatch } from 'core/store';

import Button from './Button';

const CancelButton: FC<
  DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>
> = (props) => {
  const dispatch = useDispatch<AppDispatch>();

  const onClick = useMemo(() => () => dispatch(hideModal()), [dispatch]);

  return (
    <Button {...props} extensive color="primary" type="reset" onClick={onClick}>
      Отмена
    </Button>
  );
};

export default CancelButton;
