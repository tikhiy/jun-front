import { ButtonHTMLAttributes, DetailedHTMLProps } from 'react';
import styled, { css } from 'styled-components';

import { SM } from 'core/breakpoint/useBreakpoint';
import withBreakpoint, {
  BreakpointProps,
} from 'core/breakpoint/withBreakpoint';
import { Color } from 'core/theme/DefaultTheme';

type P = { color: Color; extensive?: boolean } & DetailedHTMLProps<
  ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
>;

const Button = withBreakpoint<P & BreakpointProps>(
  styled.button<P & BreakpointProps>(
    ({ theme, color, extensive, breakpoint }) => css`
      ${theme.bordered(color, 0)}
      ${theme.rounded()}

    ${() => {
        if (extensive) {
          if (breakpoint < SM) {
            return css`
              display: block;
              margin: ${theme.spacing(1)};
              width: calc(100% - ${theme.spacing(2)});
            `;
          }

          return css`
            margin: ${theme.spacing(1)} 0 ${theme.spacing(1)}
              ${theme.spacing(1)};

            &:last-child {
              margin: ${theme.spacing(1)};
            }
          `;
        }

        return '';
      }}

      background-color: ${theme.color(color, 'enabled')};
      padding: 0 ${theme.spacing(2)};
      color: ${theme.color(color)};
      line-height: 36px;
      font-weight: 500;
      font-size: 0.875rem;

      &:hover,
      &:focus {
        background-color: ${theme.color(color, 'hovered')};
        border-color: ${theme.color(color, 'hovered')};
      }

      &:active {
        background-color: ${theme.color(color, 'pressed')};
        border-color: ${theme.color(color, 'pressed')};
      }
    `
  )
);

export default Button;
