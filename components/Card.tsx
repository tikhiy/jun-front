import styled, { css } from 'styled-components';

import { SM } from 'core/breakpoint/useBreakpoint';
import withBreakpoint, {
  BreakpointProps,
} from 'core/breakpoint/withBreakpoint';

const Card = withBreakpoint<BreakpointProps>(styled.div<BreakpointProps>`
  ${({ theme }) => theme.bordered('border')}
  ${({ theme }) => theme.rounded()}
  background-color: ${({ theme }) => theme.color('fg')};

  ${({ theme, breakpoint }) => {
    if (breakpoint < SM) {
      return css`
        margin: ${theme.spacing(2)};
        flex-direction: row;
        flex-wrap: wrap;

        & * {
          flex-basis: 100%;
        }
      `;
    }

    return '';
  }}
`);

export default Card;
