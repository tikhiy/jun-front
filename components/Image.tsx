import React, { DetailedHTMLProps, FC, ImgHTMLAttributes } from 'react';
import styled from 'styled-components';

const StyledImage = styled.div`
  overflow: hidden;
  border-radius: 40px;
  height: 40px;
  width: 40px;
  float: left;
`;

const Image: FC<
  DetailedHTMLProps<ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>
> = (attributes) => (
  <StyledImage>
    <img {...attributes} width={40} />
  </StyledImage>
);

export default Image;
