import React, { DetailedHTMLProps, FC, HTMLAttributes } from 'react';
import styled, { css } from 'styled-components';

import useBreakpointSwitch from 'core/breakpoint/useBreakpointSwitch';

const StyledGrid = styled.div<{ width?: number }>`
  ${({ width }): string => (width ? `width: ${width}px;` : '')}

  margin: 0 auto;
`;

const StyledCell = styled.div<{ width: number }>(
  ({ width, theme }) => css`
    flex-basis: ${width}%;
    max-width: ${width}%;

    &:not(:first-child) {
      padding-left: ${theme.spacing(1)};
    }

    &:not(:last-child) {
      padding-right: ${theme.spacing(1)};
    }
  `
);

const StyledRow = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const Grid: FC<
  Omit<DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>, 'ref'>
> = ({ children, ...props }) => {
  const width = useBreakpointSwitch<number>({
    xl: 1140,
    lg: 960,
    md: 720,
    sm: 540,
  });

  return (
    <StyledGrid width={width} {...props}>
      {children}
    </StyledGrid>
  );
};

const Cell: FC<{
  xs?: number;
  sm?: number;
  md?: number;
  lg?: number;
  xl?: number;
}> = ({ xs = 12, sm, md, lg, xl, children }) => {
  const fraction = useBreakpointSwitch<number>({
    xl,
    lg,
    md,
    sm,
    xs,
  }) as typeof xs;

  return <StyledCell width={(100 / 12) * fraction}>{children}</StyledCell>;
};

export { StyledRow as Row, Grid, Cell };
