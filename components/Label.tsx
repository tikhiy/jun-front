import styled, { css } from 'styled-components';

const Label = styled.label(
  ({ theme }) => css`
    margin: ${theme.spacing(1)};
    width: calc(100% - ${theme.spacing(2)});
  `
);

export default Label;
