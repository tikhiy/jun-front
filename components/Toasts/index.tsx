import React, { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { removeToast } from 'core/features/toasts/actions';
import { Toast as IToast, ToastsState } from 'core/features/toasts/types';
import { AppDispatch, RootState } from 'core/store';

import { StyledToast, StyledToastWrapper, StyledToasts } from './styled';

const Toasts: FC = () => {
  const toasts = useSelector<RootState, ToastsState>(({ toasts }) => toasts);

  const rendered = toasts.map((toast) => (
    <Toast key={toast.id} toast={toast} />
  ));

  return <StyledToasts>{rendered}</StyledToasts>;
};

const Toast: FC<{ toast: IToast }> = ({ toast }) => {
  const dispatch = useDispatch<AppDispatch>();

  useEffect(() => {
    const timeout = setTimeout(() => dispatch(removeToast(toast)), 2000);
    return () => clearTimeout(timeout);
  });

  return (
    <StyledToastWrapper>
      <StyledToast color={toast.color}>{toast.textContent}</StyledToast>
    </StyledToastWrapper>
  );
};

export default Toasts;
