import styled, { css } from 'styled-components';

import { Grid } from 'components/Grid';

import { SM } from 'core/breakpoint/useBreakpoint';
import withBreakpoint, {
  BreakpointProps,
} from 'core/breakpoint/withBreakpoint';
import { Color } from 'core/theme/DefaultTheme';

const StyledToasts = withBreakpoint<BreakpointProps>(
  styled(Grid)<BreakpointProps>(
    ({ theme, breakpoint }) => css`
      ${() => {
        if (breakpoint < SM) {
          return css`
            right: ${theme.spacing(1)};
            left: ${theme.spacing(1)};
          `;
        }

        return css`
          right: 0;
          left: 0;
        `;
      }}

      position: fixed;
      bottom: ${theme.spacing(1)};
      z-index: 3;
    `
  )
);

const StyledToastWrapper = withBreakpoint<BreakpointProps>(
  styled.div<BreakpointProps>(
    ({ theme, breakpoint }) => css`
      ${() => {
        if (breakpoint < SM) {
          return css`
            padding: ${theme.spacing(1)};
          `;
        }

        return css`
          padding: ${theme.spacing(1)} 0;
        `;
      }}

      clear: both;
    `
  )
);

const StyledToast = withBreakpoint<{ color: Color } & BreakpointProps>(
  styled.div<{ color: Color } & BreakpointProps>(
    ({ theme, breakpoint, color }) => css`
      ${theme.bordered(color)}
      ${theme.rounded()}

    ${() => {
        if (breakpoint < SM) {
          return css`
            width: 100%;
          `;
        }

        return css`
          width: 256px;
        `;
      }}

    background-color: ${theme.color('toast')};
      padding: ${theme.spacing(1)};
      float: right;
      clear: both;
      font-size: 0.875rem;
    `
  )
);

export { StyledToasts, StyledToastWrapper, StyledToast };
