import React, { FC } from 'react';
import { useSelector } from 'react-redux';
import styled, { css } from 'styled-components';

import { SM } from 'core/breakpoint/useBreakpoint';
import withBreakpoint, {
  BreakpointProps,
} from 'core/breakpoint/withBreakpoint';
import { ModalsVariant } from 'core/features/modals/types';
import { RootState } from 'core/store';

const StyledModal = withBreakpoint<BreakpointProps>(
  styled.div<BreakpointProps>`
    position: fixed;
    z-index: 2;

    ${({ theme, breakpoint }) => {
      if (breakpoint < SM) {
        return css`
          bottom: ${theme.spacing(1)};
          right: ${theme.spacing(1)};
          left: ${theme.spacing(1)};
        `;
      }

      return css`
        bottom: 12.5%;
      `;
    }}
  `
);

const Modal: FC<{ variant: ModalsVariant }> = ({ children, variant: type }) => {
  const active = useSelector<RootState, boolean>(
    ({ modals }) => modals[modals.length - 1] === type
  );

  if (active) {
    return <StyledModal>{children}</StyledModal>;
  }

  return null;
};

export default Modal;
