import React, { FC, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import CancelButton from 'components/CancelButton';
import Headline from 'components/Headline';
import Modal from 'components/Modal';

import { EmployeeState } from 'core/features/employee/types';
import { deleteEmployee } from 'core/features/employees/thunks';
import { AppDispatch, RootState } from 'core/store';

import DeleteButton from '../buttons/DeleteButton';

const DeleteEmployeeModal: FC = () => {
  const employee = useSelector<RootState, EmployeeState>(
    ({ employee }) => employee
  );
  const dispatch = useDispatch<AppDispatch>();

  const onSubmit = useMemo(
    () => (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();

      if (employee) {
        // eslint-disable-next-line
        // @ts-ignore
        dispatch(deleteEmployee(employee.id));
      }
    },
    [dispatch, employee]
  );

  return (
    <Modal variant="DELETE_EMPLOYEE_MODAL">
      <Headline>
        Удалить сотрудника {employee?.firstName} {employee?.lastName}?
      </Headline>

      <form method="DELETE" onSubmit={onSubmit}>
        <DeleteButton />
        <CancelButton autoFocus />
      </form>
    </Modal>
  );
};

export default DeleteEmployeeModal;
