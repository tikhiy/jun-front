import React, { FC, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import CancelButton from 'components/CancelButton';
import Headline from 'components/Headline';
import Input from 'components/Input';
import Label from 'components/Label';
import Modal from 'components/Modal';

import { EmployeeState } from 'core/features/employee/types';
import { updateEmployee } from 'core/features/employees/thunks';
import { addToast } from 'core/features/toasts/actions';
import { AppDispatch, RootState } from 'core/store';

import UpdateButton from '../buttons/UpdateButton';
import schema from '../schema';

const UpdateEmployeeModal: FC = () => {
  const employee = useSelector<RootState, EmployeeState>(
    ({ employee }) => employee
  );
  const dispatch = useDispatch<AppDispatch>();

  const onSubmit = useMemo(
    () => (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();

      if (employee) {
        // eslint-disable-next-line
        // @ts-ignore
        const firstName = event.target.firstName.value.trim();
        // eslint-disable-next-line
        // @ts-ignore
        const lastName = event.target.lastName.value.trim();

        const payload = {
          firstName,
          lastName,
        };

        const { error } = schema.validate(payload);

        if (error) {
          dispatch(addToast(error.message, 'danger'));
        } else {
          // eslint-disable-next-line
          // @ts-ignore
          dispatch(updateEmployee({ id: employee.id, ...payload }));
        }
      }
    },
    [dispatch, employee]
  );

  return (
    <Modal variant="UPDATE_EMPLOYEE_MODAL">
      <Headline>Изменить данные сотрудника</Headline>

      <form method="PUT" onSubmit={onSubmit}>
        <Label htmlFor="update-employee-modal-first-name">Имя</Label>
        <Input
          name="firstName"
          id="update-employee-modal-first-name"
          defaultValue={employee?.firstName}
          autoFocus
        />
        <Label htmlFor="update-employee-modal-last-name">Фамилия</Label>
        <Input
          name="lastName"
          id="update-employee-modal-last-name"
          defaultValue={employee?.lastName}
        />
        <UpdateButton />
        <CancelButton />
      </form>
    </Modal>
  );
};

export default UpdateEmployeeModal;
