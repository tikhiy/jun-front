import React, { FC } from 'react';
import { useDispatch } from 'react-redux';

import Button from 'components/Button';
import CancelButton from 'components/CancelButton';
import Headline from 'components/Headline';
import Modal from 'components/Modal';

import { hideModal, showModal } from 'core/features/modals/actions';
import { ModalsVariant } from 'core/features/modals/types';
import { AppDispatch } from 'core/store';

const EmployeeActionsModal: FC = () => {
  const dispatch = useDispatch<AppDispatch>();

  const onClick = (variant: ModalsVariant) => () => {
    dispatch(hideModal());
    dispatch(showModal(variant));
  };
  return (
    <Modal variant="EMPLOYEE_ACTIONS_MODAL">
      <Headline>Выберите действие</Headline>
      <Button
        extensive
        onClick={onClick('UPDATE_EMPLOYEE_MODAL')}
        color="warning"
        autoFocus
      >
        Изменить данные
      </Button>
      <Button
        extensive
        onClick={onClick('DELETE_EMPLOYEE_MODAL')}
        color="danger"
      >
        Удалить
      </Button>
      <CancelButton />
    </Modal>
  );
};

export default EmployeeActionsModal;
