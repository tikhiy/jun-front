import React, { FC, useMemo } from 'react';
import { useDispatch } from 'react-redux';

import CancelButton from 'components/CancelButton';
import Headline from 'components/Headline';
import Input from 'components/Input';
import Label from 'components/Label';
import Modal from 'components/Modal';

import { createEmployee } from 'core/features/employees/thunks';
import { addToast } from 'core/features/toasts/actions';
import { AppDispatch } from 'core/store';

import CreateButton from '../buttons/CreateButton';
import schema from '../schema';

const CreateEmployeeModal: FC = () => {
  const dispatch = useDispatch<AppDispatch>();

  const onSubmit = useMemo(
    () => (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      // eslint-disable-next-line
      // @ts-ignore
      const firstName: string = event.target.firstName.value.trim();
      // eslint-disable-next-line
      // @ts-ignore
      const lastName: string = event.target.lastName.value.trim();

      const payload = {
        firstName,
        lastName,
      };

      const { error } = schema.validate(payload);

      if (error) {
        dispatch(addToast(error.message, 'danger'));
      } else {
        // eslint-disable-next-line
        // @ts-ignore
        dispatch(createEmployee(payload));
      }
    },
    [dispatch]
  );

  return (
    <Modal variant="CREATE_EMPLOYEE_MODAL">
      <Headline>Добавить сотрудника</Headline>

      <form method="POST" onSubmit={onSubmit}>
        <Label htmlFor="create-employee-modal-first-name">Имя</Label>
        <Input
          name="firstName"
          id="create-employee-modal-first-name"
          autoFocus
        />
        <Label htmlFor="create-employee-modal-last-name">Фамилия</Label>
        <Input name="lastName" id="create-employee-modal-last-name" />
        <CreateButton />
        <CancelButton />
      </form>
    </Modal>
  );
};

export default CreateEmployeeModal;
