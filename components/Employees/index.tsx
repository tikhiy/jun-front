import React, { FC } from 'react';

import Card from 'components/Card';

import EmployeesList from './EmployeesList';
import CreateEmployeeModal from './modals/CreateEmployeeModal';
import DeleteEmployeeModal from './modals/DeleteEmployeeModal';
import EmployeeActionsModal from './modals/EmployeeActionsModal';
import UpdateEmployeeModal from './modals/UpdateEmployeeModal';

const Employees: FC = () => (
  <Card>
    <EmployeesList />
    <EmployeeActionsModal />
    <CreateEmployeeModal />
    <UpdateEmployeeModal />
    <DeleteEmployeeModal />
  </Card>
);

export default Employees;
