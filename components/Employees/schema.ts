import Joi from 'joi';

const nameSchema = Joi.string()
  .required()
  .empty()
  .min(2)
  .max(16)
  .pattern(/^[a-zA-Zа-яА-Я ,.'-]+$/);

const schema = Joi.object({
  firstName: nameSchema.messages({
    'string.empty': 'Имя не может быть пустым.',
    'string.min': 'Имя не может быть короче двух символов.',
    'string.max': 'Имя не может быть длиннее шестнадцати символов.',
    'string.pattern.base': 'Имя не может содержать недопустимые символы.',
  }),

  lastName: nameSchema.messages({
    'string.empty': 'Фамилия не может быть пустой.',
    'string.min': 'Фамилия не может быть короче двух символов.',
    'string.max': 'Фамилия не может быть длиннее шестнадцати символов.',
    'string.pattern.base': 'Фамилия не может содержать недопустимые символы.',
  }),
});

export default schema;
