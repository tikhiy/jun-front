import React, { FC, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { fetchEmployees } from 'core/features/employees/thunks';
import { EmployeesState } from 'core/features/employees/types';
import { AppDispatch, RootState } from 'core/store';

import EmployeesListItem from './EmployeesListItem';

const EmployeesList: FC = () => {
  const employees = useSelector<RootState, EmployeesState>(
    ({ employees }) => employees
  );
  const dispatch = useDispatch<AppDispatch>();

  if (employees) {
    const rendered = employees.map((employee) => (
      <EmployeesListItem key={employee.id} employee={employee} />
    ));

    return <Fragment>{rendered}</Fragment>;
  }

  // eslint-disable-next-line
  // @ts-ignore
  dispatch(fetchEmployees());
  return null;
};

export default EmployeesList;
