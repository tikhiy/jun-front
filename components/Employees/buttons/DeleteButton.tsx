import React, { ButtonHTMLAttributes, DetailedHTMLProps, FC } from 'react';

import Button from 'components/Button';

const DeleteButton: FC<
  DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>
> = (props) => (
  <Button {...props} extensive color="danger" type="submit">
    Удалить
  </Button>
);

export default DeleteButton;
