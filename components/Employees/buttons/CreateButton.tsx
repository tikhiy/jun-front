import React, { ButtonHTMLAttributes, DetailedHTMLProps, FC } from 'react';

import Button from 'components/Button';

const CreateButton: FC<
  DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>
> = (props) => (
  <Button {...props} extensive color="success" type="submit">
    Добавить
  </Button>
);

export default CreateButton;
