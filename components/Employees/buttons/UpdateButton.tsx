import React, { ButtonHTMLAttributes, DetailedHTMLProps, FC } from 'react';

import Button from 'components/Button';

const UpdateButton: FC<
  DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>
> = (props) => (
  <Button {...props} extensive color="warning" type="submit">
    Применить изменения
  </Button>
);

export default UpdateButton;
