import React, { FC, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import styled, { css } from 'styled-components';

import Image from 'components/Image';

import { Employee } from 'core/Employee';
import { setEmployee } from 'core/features/employee/actions';
import { showModal } from 'core/features/modals/actions';
import { AppDispatch } from 'core/store';

const StyledEmployeesListItem = styled.button`
  display: block;
  padding: 8px;
  height: 56px;
  width: 100%;
  line-height: 40px;
  text-align: left;

  &:hover,
  &:focus {
    background-color: ${({ theme }) => theme.color('bg', 'hovered')};
  }

  ${({ theme }) =>
    css`
      border-bottom: ${theme.borderWidth} solid ${theme.color('border')};
    `}

  &:last-child {
    border-bottom: none;
  }
`;

const EmployeesListItem: FC<{ employee: Employee }> = ({ employee }) => {
  const dispatch = useDispatch<AppDispatch>();

  const onClick = useMemo(
    () => () => {
      dispatch(setEmployee(employee));
      dispatch(showModal('EMPLOYEE_ACTIONS_MODAL'));
    },
    [dispatch, employee]
  );

  return (
    <StyledEmployeesListItem onClick={onClick}>
      <Image
        alt="Фото сотрудника"
        src="https://source.unsplash.com/random/256x256"
      />
      <span style={{ marginLeft: 16 }}>
        {employee.firstName} {employee.lastName}
      </span>
    </StyledEmployeesListItem>
  );
};

export default EmployeesListItem;
