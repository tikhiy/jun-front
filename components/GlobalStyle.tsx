import { createGlobalStyle, css } from 'styled-components';

const GlobalStyle = createGlobalStyle(
  ({ theme }) => css`
    *,
    ::before,
    ::after {
      box-sizing: border-box;
    }

    button,
    input,
    label,
    html {
      font: 400 14px / 1.5 Montserrat, sans-serif;
    }

    body {
      background-color: ${theme.color('bg')};
      padding: ${theme.spacing(2)} 0;
      margin: 0;
      color: ${theme.color('highEmphasis')};
    }

    button,
    input {
      background: none;
      outline: none;
      border: none;
      color: ${theme.color('highEmphasis')};
    }

    button {
      cursor: pointer;
    }
  `
);

export default GlobalStyle;
