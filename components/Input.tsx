import { DetailedHTMLProps, InputHTMLAttributes } from 'react';
import styled, { css } from 'styled-components';

import { SM } from 'core/breakpoint/useBreakpoint';
import withBreakpoint, {
  BreakpointProps,
} from 'core/breakpoint/withBreakpoint';

type P = DetailedHTMLProps<
  InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

const Input = withBreakpoint<P & BreakpointProps>(
  styled.input<P & BreakpointProps>(
    ({ breakpoint, theme }) => css`
      ${theme.bordered('border')}
      ${theme.rounded()}
      ${breakpoint < SM
        ? css`
            width: 100%;
          `
        : ''}

      padding: 0 ${theme.spacing(2)};
      display: block;
      margin: ${theme.spacing(1)};
      height: 36px;
      width: calc(100% - ${theme.spacing(2)});
      line-height: 36px;
      font-weight: 400;
      font-size: 0.875rem;

      &:focus {
        border-color: ${theme.color('mediumEmphasis')};
      }
    `
  )
);

export default Input;
