'use strict';

/** @typedef {import('webpack').Configuration & { devServer: import('webpack-dev-server').Configuration }} Configuration */
/** @typedef {import('webpack').CliConfigOptions & import('webpack').Configuration & import('webpack-dev-server').Configuration} Argv */

/**
 * @callback ConfigurationFactory
 * @param {any} _
 * @param {Argv} argv
 * @return {Configuration}
 */

const { resolve } = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { DefinePlugin } = require('webpack');

const isDev = process.env.NODE_ENV !== 'production';

/**
 * @type {ConfigurationFactory}
 */
module.exports = () => {
  /**
   * @type {Configuration['optimization']}
   */
  const optimization = {};

  if (isDev) {
    optimization.removeAvailableModules = false;
    optimization.removeEmptyChunks = false;
    optimization.splitChunks = false;
  }

  /**
   * @type {{ [k: string]: string }}
   */
  const bundleEnv = {};

  if (!isDev) {
    bundleEnv['process.env.NODE_ENV'] = '"production"';
  }

  bundleEnv.BASE_URL = isDev ? 'http://127.0.0.1:3000/api/v1' : '/api/v1';

  return {
    output: {
      path: resolve('./dist'),
      filename: '[fullhash].js',
      publicPath: '/',
      pathinfo: false,
    },

    plugins: [
      new HtmlWebpackPlugin({
        template: require.resolve('./src/index.html'),
      }),

      new ForkTsCheckerWebpackPlugin(),
      new CleanWebpackPlugin(),

      new DefinePlugin({
        'process.env': JSON.stringify(bundleEnv),
      }),
    ],

    module: {
      rules: [
        {
          test: /\.[tj]sx?$/,
          exclude: /node_modules\/(?!(joi|@sideways)\/).*/,

          use: [
            {
              loader: 'babel-loader',
            },
          ],
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
      ],
    },

    mode: isDev ? 'development' : 'production',

    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx'],

      alias: {
        components: resolve('components'),
        core: resolve('core'),
      },
    },

    watchOptions: {
      aggregateTimeout: 2000,
    },

    devServer: {
      historyApiFallback: true,

      headers: {
        'Access-Control-Allow-Origin': '',
      },
    },

    optimization,
  };
};
