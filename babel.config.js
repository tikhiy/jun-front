'use strict';

/**
 * @type {import('@babel/core').TransformOptions}
 */
module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        useBuiltIns: 'entry',
        modules: false,
        corejs: 3,
      },
    ],

    '@babel/preset-typescript',
    '@babel/preset-react',
  ],

  plugins: ['@babel/plugin-transform-runtime'],

  sourceType: 'unambiguous',
};
